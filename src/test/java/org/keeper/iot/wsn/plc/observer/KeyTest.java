/**
 * 
 */
package org.keeper.iot.wsn.plc.observer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;
import org.keeper.utils.Helpers;

/**
 * @author keeper
 *
 */
public class KeyTest {
  /**
   * Test method for {@link org.keeper.iot.wsn.plc.observer.Key#Key(java.net.InetAddress)}.
   */
  @Test
  public void testKeyInetAddress() {
    InetAddress addr = null;
    try {
      addr =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    Key k_addr = new Key(addr);
    assertEquals(0xF0F0F0F0, k_addr.hashCode());

    InetAddress addr2 = null;
    try {
      addr2 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0x0F, (byte) 0x0F});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }

    Key k_addr2 = new Key(addr2);
    assertEquals(0x00F000F0, k_addr2.hashCode());

    InetAddress addr3 = null;
    try {
      addr3 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xF0, (byte) 0xF0});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }

    Key k_addr3 = new Key(addr3);
    assertEquals(0xF000F000, k_addr3.hashCode());

    InetAddress addr4 = null;
    try {
      addr4 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0x0A, (byte) 0x09});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }

    Key k_addr4 = new Key(addr4);
    assertEquals(0x00A00090, k_addr4.hashCode());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.observer.Key#Key(int)}.
   */
  @Test
  public void testKeyInt() {
    Key k = new Key(0);
    assertEquals(k.hashCode(), 0);

    Key k_msgId = new Key(0x12345678);
    assertEquals(0x12345678, k_msgId.hashCode());

    Key k_msgId2 = new Key(0xFFFFFFFF);
    assertEquals(0xFFFFFFFF, k_msgId2.hashCode());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.observer.Key#Key(java.net.InetAddress, int)}.
   */
  @Test
  public void testKeyInetAddressInt() {
    InetAddress addr = null;
    try {
      addr =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    int msgId = 0x6205060F; // example message id
    Key k_addr = new Key(addr, msgId);
    assertEquals(
        "Expected:" + Helpers.intToHexString(msgId + 0xF0F0F0F0) + " but was:"
            + Helpers.intToHexString(k_addr.hashCode()), msgId + 0xF0F0F0F0, k_addr.hashCode());

    InetAddress addr2 = null;
    try {
      addr2 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    int msgId2 = 0x0205060F; // example message id, not realist.
    Key k_addr2 = new Key(addr2, msgId2);
    assertEquals(
        "Expected:" + Helpers.intToHexString(msgId2 + 0xF0F0F0F0) + " but was:"
            + Helpers.intToHexString(k_addr2.hashCode()), msgId2 + 0xF0F0F0F0, k_addr2.hashCode());

    InetAddress addr3 = null;
    try {
      addr3 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0x0F, (byte) 0x0F});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    int msgId3 = 0x0205060F; // example message id.
    Key k_addr3 = new Key(addr3, msgId3);
    assertEquals(
        "Expected:" + Helpers.intToHexString(msgId3 + 0x00F000F0) + " but was:"
            + Helpers.intToHexString(k_addr3.hashCode()), msgId3 + 0x00F000F0, k_addr3.hashCode());

    InetAddress addr4 = null;
    try {
      addr4 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xF0, (byte) 0xF0});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    int msgId4 = 0x0205060F; // example message id.
    Key k_addr4 = new Key(addr4, msgId4);
    assertEquals(
        "Expected:" + Helpers.intToHexString(msgId4 + 0xF000F000) + " but was:"
            + Helpers.intToHexString(k_addr4.hashCode()), msgId4 + 0xF000F000, k_addr4.hashCode());

    InetAddress addr5 = null;
    try {
      addr5 =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0x73, (byte) 0x68});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    int msgId5 = 0x0205060F; // example message id.
    Key k_addr5 = new Key(addr5, msgId5);
    assertEquals(
        "Expected:" + Helpers.intToHexString(msgId5 + 0x70306080) + " but was:"
            + Helpers.intToHexString(k_addr5.hashCode()), msgId5 + 0x70306080, k_addr5.hashCode());
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.observer.Key#equals(java.lang.Object)}.
   */
  @Test
  public void testEqualsObject() {
    int msgId = 0x6205061F; // example message id
    Key k1 = new Key(msgId);

    InetAddress addr = null;
    try {
      addr =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    Key k2 = new Key(addr);

    Key k3 = new Key(addr, msgId);

    assertNotEquals(k1, k2);
    assertNotEquals(k1, k3);
    assertNotEquals(k2, k3);

    InetAddress addr2 = null;
    try {
      addr2 =
          InetAddress.getByAddress(new byte[] {(byte) 0x00, (byte) 0x00, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }

    Key k4 = new Key(addr2);

    Key k5 = new Key(addr2, msgId);

    assertNotEquals(k4, k2);
    assertEquals(k2.hashCode(), k4.hashCode());
    assertNotEquals(k5, k3);
    assertEquals(k5.hashCode(), k3.hashCode());

    Key k6 = new Key(addr2, msgId);
    assertEquals(k5, k6);
  }

  /**
   * Test method for {@link org.keeper.iot.wsn.plc.observer.Key#toString()}.
   */
  @Test
  public void testToString() {
    int msgId = 0x6205061F; // example message id
    Key k1 = new Key(msgId);

    InetAddress addr = null;
    try {
      addr =
          InetAddress.getByAddress(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }
    Key k2 = new Key(addr);

    Key k3 = new Key(addr, msgId);

    InetAddress addr2 = null;
    try {
      addr2 =
          InetAddress.getByAddress(new byte[] {(byte) 0x00, (byte) 0x00, (byte) 0xFF, (byte) 0xFF});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }

    Key k4 = new Key(addr2);

    Key k5 = new Key(addr2, msgId);

    InetAddress addr3 = null;
    try {
      addr3 =
          InetAddress.getByAddress(new byte[] {(byte) 0x12, (byte) 0x34, (byte) 0xab, (byte) 0xcd});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Shouldn't happen");
    }

    Key k6 = new Key(addr3);

    Key k7 = new Key(addr3, msgId);

    System.out.println(k1.toString());
    System.out.println(k2.toString());
    System.out.println(k3.toString());
    System.out.println(k4.toString());
    System.out.println(k5.toString());
    System.out.println(k6.toString());
    System.out.println(k7.toString());

    assertEquals(k1.toString(), "Key(000000006205061F), Hash(6205061F)");
    assertEquals(k2.toString(), "Key(FFFFFFFF00000000), Hash(F0F0F0F0)");
    assertEquals(k3.toString(), "Key(FFFFFFFF6205061F), Hash(52F5F70F)");
    assertEquals(k4.toString(), "Key(0000FFFF00000000), Hash(F0F0F0F0)");
    assertEquals(k5.toString(), "Key(0000FFFF6205061F), Hash(52F5F70F)");
    assertEquals(k6.toString(), "Key(1234ABCD00000000), Hash(A0B0C0D0)");
    assertEquals(k7.toString(), "Key(1234ABCD6205061F), Hash(02B5C6EF)");
  }

}
