package org.keeper.iot.wsn.plc.messages.sys;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import com.esotericsoftware.minlog.Log;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SysGetManufacturerIdResponseTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }

  class CustomHandler implements Sys {
    public int status = 0;
    public InetAddress address = null;
    public byte[] manufacturerId = null;

    @Override
    public void onSysGetVersionResponse(InetAddress address, int status, byte version) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetManufacturerIdResponse(InetAddress address, int status,
        byte[] manufacturerId) {
      this.address = address;
      this.status = status;
      this.manufacturerId = manufacturerId;
    }

    @Override
    public void onSysGetModelIdResponse(InetAddress address, int status, byte[] modelId) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetHardwareRevisionResponse(InetAddress address, int status, byte[] hwVersion) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetSerialResponse(InetAddress address, int status, byte[] serial) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetFirmwareVersionResponse(InetAddress address, int status, byte[] fwVersion) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetDateResponse(InetAddress address, int status, byte[] date) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysSetDateResponse(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysUpdateFirmwareResponse(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysRebootResponse(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetTemperature(InetAddress address, int status, byte temperatureCelsius) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysGetPowerSource(InetAddress address, int status, byte powerSource) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysFactoryReset(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysUserDidSomething(InetAddress address, int status, int actions) {
      fail("Shouldn't happen");
    }

    @Override
    public void onSysFirmwareChunkUpload(InetAddress address, int status) {
      fail("Shouldn't happen");
    }

  }

  @Test
  public void test() {
    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }

    byte[] data =
        {0x1c, 0x00, (byte) 0xfe, 0x61, 0x01, 0x01, 0x00, 0x00, 0x00, 0x45, 0x64, 0x50, 0x2d, 0x57,
            0x49, 0x54, 0x48, 0x55, 0x53, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, (byte) 0xc7, 0x00, (byte) 0xde, (byte) 0xad}; // captured from udp, last three
                                                                // bytes are bogus.

    byte[] data2 =
        {0x1c, 0x00, (byte) 0xfe, 0x61, 0x01, 0x01, 0x00, 0x00, 0x00, 0x45, 0x64, 0x50, 0x2d, 0x57,
            0x49, 0x54, 0x48, 0x55, 0x53, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, (byte) 0xc7}; // Without bogus bytes.
    
    byte[] manId = {0x45, 0x64, 0x50, 0x2d, 0x57,
        0x49, 0x54, 0x48, 0x55, 0x53, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00};

    Message msg = new SysGetManufacturerIdResponse(address, data);

    assertTrue(Helpers.compareArrays(data2, msg.getRaw()));
    assertTrue(MessageDebugHelpers.isSameAddresses("test", address, msg.getAddress()));
    
    CustomHandler handler = new CustomHandler();
    msg.callHandler(handler);
    
    assertTrue(MessageDebugHelpers.isSameAddresses("test", handler.address, msg.getAddress()));
    assertEquals(1, handler.status);
    assertTrue(Helpers.compareArrays(manId, handler.manufacturerId));
  }

}
