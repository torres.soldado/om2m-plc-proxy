package iot.keeper.proxies.om2m_plc_proxy;

import static com.esotericsoftware.minlog.Log.error;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.keeper.iot.wsn.plc.containers.IDeviceObserver;
import org.keeper.iot.wsn.plc.containers.IContainer;
import org.keeper.iot.wsn.plc.containers.PlcResource;
import org.keeper.iot.wsn.plc.containers.SuperSpawner;
import org.keeper.iot.wsn.plc.core.Core;
import org.keeper.iot.wsn.plc.core.ICore;

import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;

public class CoapResourceSpawner implements IDeviceObserver {
  private final String TAG = CoapResourceSpawner.class.getSimpleName();
  @SuppressWarnings("unused")
  private final SuperSpawner spawner;
  private final CoapServer coapServer;
  private ICore plcCore;

  public CoapResourceSpawner(CoapServer server) {
    this.coapServer = server;

    plcCore = null;
    try {
      plcCore = new Core();
    } catch (SocketException e2) {
      error(TAG, "Error creating plc Core, socket error:" + e2.getMessage());
    }

    this.spawner = new SuperSpawner(plcCore, this);

//    byte[] data =
//        {0x6b, 0x00, (byte) 0xfe, 0x65, 0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x02, 0x03, 0x01, 0x02,
//            0x05, 0x07, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x04, 0x00, 0x01, 0x04, 0x05,
//            0x00, 0x01, 0x00, 0x02, 0x00, 0x01, 0x03, 0x00, 0x01, 0x02, 0x02, 0x00, 0x03, 0x00,
//            0x0e, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x67, 0x68,
//            0x6d, 0x02, 0x00, 0x03, 0x00, 0x07, 0x06, 0x09, 0x56, 0x57, 0x61, 0x62, 0x6a, 0x02,
//            0x00, 0x03, 0x00, 0x07, 0x07, 0x0a, 0x58, 0x59, 0x63, 0x64, 0x6b, 0x02, 0x00, 0x03,
//            0x00, 0x07, 0x08, 0x0b, 0x5a, 0x5b, 0x65, 0x66, 0x6c, 0x02, 0x00, 0x02, 0x00, 0x02,
//            0x00, 0x01, 0x02, 0x00, 0x02, 0x00, 0x02, 0x00, 0x01, 0x00, (byte) 0xfc};
//
//    InetAddress address = null;
//    try {
//      address = InetAddress.getByName("192.168.100.1");
//    } catch (UnknownHostException e) {
//      e.printStackTrace();
//      error("Should not happen");
//    }
//
//    AppGenericsGetCapabilitiesResponse msg = new AppGenericsGetCapabilitiesResponse(address, data);
//    
//    spawner.onAppGenericsCapabilities(address, 1, msg.getCapabilities());
  }

  @Override
  public void onNewDevice(IContainer newManager) {
    PlcResource root = newManager.getRoot();
    coapServer.add(plcToCoapResource(root, null));
  }

  private CoapResource plcToCoapResource(PlcResource plcRes, CoapResource parent) {
    CoapResource newCoapResource = new CoapResource(plcRes.getName()) {
      @Override
      public void handleGET(CoapExchange exchange) {
        exchange.respond(plcRes.getValue());
      }

      @Override
      public void handlePOST(CoapExchange exchange) {
        try {
          String param = new String(exchange.getRequestPayload(), "UTF-8");
          List<String> items = Arrays.asList(param.split("\\s*;\\s*"));
          if (plcRes.remoteSet(items)) {
            exchange.respond(ResponseCode.VALID);
          } else {
            exchange.respond(ResponseCode.BAD_REQUEST);
          }
        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
          exchange.respond(ResponseCode.BAD_REQUEST, "Invalid String");
        }
      }
    };

    if (null != parent) {
      parent.add(newCoapResource);
    }

    List<PlcResource> children = plcRes.getChildren();
    if (null != children) {
      for (PlcResource child : children) {
        plcToCoapResource(child, newCoapResource);
      }
    }

    return newCoapResource;
  }
}
