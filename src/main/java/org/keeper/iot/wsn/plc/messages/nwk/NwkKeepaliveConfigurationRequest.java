/**
 * Creates a network keepalive configuration request {@link Message} from parameters.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class NwkKeepaliveConfigurationRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkKeepaliveConfigurationRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param timer0 ??
   * @param timer1 ??
   * @param configuration ??
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte[] timer0, byte[] timer1,
      byte configuration) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_KEEPALIVE_CFG, 0, 0, 0, 0, 0, 0};
    Helpers.arrayToArray(timer0, messageData, 0, 2, 5);
    Helpers.arrayToArray(timer1, messageData, 0, 2, 7);
    messageData[9] = configuration;
    Message message = new NwkKeepaliveConfigurationRequest(address, messageData);

    return message;
  }

  /**
   * Message constructor.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private NwkKeepaliveConfigurationRequest(InetAddress address, byte[] data) {
    super(address, data, NwkKeepaliveConfigurationResponse.REGISTRATION_ID);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }
}
