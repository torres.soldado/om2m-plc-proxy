/**
 * Creates a modbus get version request {@link Message} from parameters.
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.mb;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class MbGetVersionRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = MbGetVersionRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_MB,
            Mb.MB_GET_VERSION, 0};
    Message message = new MbGetVersionRequest(address, messageData);

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private MbGetVersionRequest(InetAddress address, byte[] data) {
    super(address, data, MbGetVersionResponse.REGISTRATION_ID);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + super.toString();
  }
}
