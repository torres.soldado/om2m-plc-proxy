/**
 * Creates a {@link Message} from raw response data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsRangeAlert extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsRangeAlert.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_GENERICS,
        AppGenerics.GEN_CMD_PERIODIC_REPORT, endpoint, 0, 0});
  }

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * Attribute cluster id.
   */
  private final byte clusterId;

  /**
   * Attribute id.
   */
  private final byte attributeId;

  /**
   * Alert type id.
   */
  private final byte alarmType;

  /**
   * Attribute data in little endian.
   */
  private final byte[] attributeData;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppGenericsRangeAlert(InetAddress address, byte[] data) throws IllegalArgumentException {
    super(address, data);
    this.clusterId = data[8];
    this.attributeId = data[9];
    this.alarmType = data[12];
    int attributeLen = data.length - 14;
    this.attributeData = new byte[attributeLen];
    Helpers.arrayToArray(data, attributeData, 13, attributeLen, 0);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 12);
  }

  /**
   * Don't allow.
   */
  private AppGenericsRangeAlert() {
    super(null, null);
    this.status = 0;
    this.clusterId = 0;
    this.attributeId = 0;
    this.alarmType = 0;
    this.attributeData = null;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppGenerics) {
      ((AppGenerics) obs).onAppGenericsRangeAlert(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.clusterId, this.attributeId,
          this.attributeData, alarmType);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printStatus(status)
        + MessageDebugHelpers.printClusterId(clusterId)
        + MessageDebugHelpers.printAttributeId(attributeId)
        + MessageDebugHelpers.printRawData(attributeData, attributeData.length)
        + AppRangeAlert.getRangeAlertTypeStr(alarmType);
  }
}
