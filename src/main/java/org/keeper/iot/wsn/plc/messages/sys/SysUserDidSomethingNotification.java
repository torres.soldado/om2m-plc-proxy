/**
 * SysUserDidSomethingNotification.java
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class SysUserDidSomethingNotification extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysUserDidSomethingNotification.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields.createId(new byte[] {4, 0,
      MessageFields.SOF, MessageFields.CMDO_AREQ | MessageFields.SUBSYSTEM_SYS,
      Sys.SYS_CMD_LOCAL_ACTION, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * The user actions bit-field.
   */
  private final byte userActions;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public SysUserDidSomethingNotification(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
    this.userActions = data[9];
  }

  /**
   * Don't allow.
   */
  private SysUserDidSomethingNotification() {
    super(null, null);
    this.status = 0;
    this.userActions = 0;
  }

  /**
   * Calls the specific handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Sys) {
      ((Sys) obs).onSysUserDidSomething(super.getAddress(), this.status, this.userActions);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + super.toString()
        + MessageDebugHelpers.printStatus(this.status)
        + " UserActions(" + Helpers.byteToHexString(this.userActions) + ")";
  }
}
