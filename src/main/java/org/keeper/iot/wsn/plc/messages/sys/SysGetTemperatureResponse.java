/**
 * SysGetTemperatureResponse.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * Implements the message parser for SysGetTemperatureResponse.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class SysGetTemperatureResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysGetTemperatureResponse.class.getSimpleName();
  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields
      .createId(new byte[] {4, 0, MessageFields.SOF,
          MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_SYS, Sys.SYS_CMD_GET_TEMP, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;
  /**
   * The device's current temperature in Celsius units.
   */
  private final byte temperatureCelsius;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public SysGetTemperatureResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
    this.temperatureCelsius = data[9];
  }

  /**
   * Don't allow.
   */
  private SysGetTemperatureResponse() {
    super(null, null);
    this.status = 0;
    this.temperatureCelsius = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Sys) {
      ((Sys) obs).onSysGetTemperature(super.getAddress(), this.status, this.temperatureCelsius);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + super.toString() 
        + MessageDebugHelpers.printStatus(this.status)
        + "Temperature(" + Helpers.byteToHexString(this.temperatureCelsius) + "℃)";
  }
}
