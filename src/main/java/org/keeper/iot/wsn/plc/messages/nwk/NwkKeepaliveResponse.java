/**
 * @author sergio.soldado@withus.pt
 */

// TODO test router, it shouldn't add this messsage to the resource queue because it has a zero
// response id.

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * Creates network respond to keepalive {@link Message} from parameters.
 * 
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class NwkKeepaliveResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkKeepaliveResponse.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        {0, 0, MessageFields.SOF, MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_KEEPALIVE, 0};
    Message message = new NwkKeepaliveResponse(address, messageData);

    return message;
  }

  /**
   * SysRebootRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private NwkKeepaliveResponse(InetAddress address, byte[] data) {
    super(address, data);
  }

  /**
   * Returns textual representation of message.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }
}
