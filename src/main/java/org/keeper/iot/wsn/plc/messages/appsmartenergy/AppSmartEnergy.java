package org.keeper.iot.wsn.plc.messages.appsmartenergy;

import java.net.InetAddress;

public interface AppSmartEnergy {
  /**
   * Response with cluster version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param endpoint Device enpoint.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppSmartEnergyGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber);

  /**
   * Response to set meter operation mode.
   * 
   * @param address Address of device that originated this message.
   * @param endpoint Device enpoint.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppSmartEnergySetMeterModeResponse(InetAddress address, byte endpoint, int status,
      byte sequenceNumber);

  /**
   * Response to get meter operation mode.
   * 
   * @param address Address of device that originated this message.
   * @param endpoint Device enpoint.
   * @param status Plc status code bitfields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppSmartEnergyGetMeterModeResponse(InetAddress address, byte endpoint, int status,
      byte mode, byte sequenceNumber);
}
