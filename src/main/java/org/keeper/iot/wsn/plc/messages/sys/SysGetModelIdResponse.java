/**
 * SysGetModelIdResponse.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author keeper Implements the message parser for SysGetModelIdResponse.
 * @version $Revision: 1.0 $
 */
public class SysGetModelIdResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysGetModelIdResponse.class.getSimpleName();
  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields.createId(new byte[] {4, 0,
      MessageFields.SOF, MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_SYS, Sys.SYS_CMD_GET_MODEL,
      0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;
  /**
   * The device's model Id.
   */
  private final byte[] modelId = new byte[SysAttributes.MODEL_LEN];

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public SysGetModelIdResponse(InetAddress address, byte[] data) throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
    Helpers.arrayToArray(data, this.modelId, 9, SysAttributes.MODEL_LEN, 0);
  }

  /**
   * Don't allow.
   */
  private SysGetModelIdResponse() {
    super(null, null);
    this.status = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Sys) {
      ((Sys) obs).onSysGetModelIdResponse(super.getAddress(), this.status, this.modelId);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + super.toString() 
        + MessageDebugHelpers.printStatus(this.status)
        + "ModelId(" + Helpers.arrayToHexString(this.modelId, this.modelId.length) + ")";
  }
}
