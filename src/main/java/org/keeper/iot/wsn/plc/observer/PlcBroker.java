/**
 * Implements the observer pattern. An observer may subscribe either to a specific message id or
 * (more strictly) to a specific device address along with a specific message id. /see
 * {@link Message#createId(byte[])}
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.observer;

import static com.esotericsoftware.minlog.Log.TRACE;
import static com.esotericsoftware.minlog.Log.debug;
import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.router.IPlcMessageListener;
import org.keeper.iot.wsn.plc.observer.IBroker;

import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class PlcBroker implements IBroker, IPlcMessageListener {
  /**
   * Just a tag for the logger.
   */
  private static final String LOG_TAG = PlcBroker.class.getSimpleName();

  /**
   * Holds references to the registered observers.
   */
  private final Map<Key, LinkedList<ObserverEntry>> observerMap =
      new ConcurrentHashMap<Key, LinkedList<ObserverEntry>>(32, 0.75f);

  /**
   * Constructor.
   */
  public PlcBroker() {}

  @Override
  protected Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException("Not supported");
  }

  /**
   * Method informObservers.
   * 
   * @param key Key
   * @param message Message
   */
  private void informObservers(Key key, Message message) {
    trace(LOG_TAG, "Informing observers: looking for " + key.toString());

    List<ObserverEntry> obsList = this.observerMap.get(key);

    if (null == obsList) {
      debug(LOG_TAG, "No observers found for " + key.toString());
      return;
    }

    List<ObserverEntry> removableEntries = new LinkedList<ObserverEntry>();
    synchronized (removableEntries) {
      for (ObserverEntry e : obsList) {
        debug(LOG_TAG, "Observers found for " + key.toString() + ", delivering message");
        e.obs.onDeviceMessage(message);
        if (e.removeWhenNotified) {
          debug(LOG_TAG, "Observer is going to be removed from list");
          removableEntries.add(e);
        }
      }

      for (ObserverEntry e : removableEntries) {
        obsList.remove(e);
      }
    }
  }

  /**
   * Method informObserversTimeout.
   * 
   * @param key Key
   * @param msgId int
   */
  private void informObserversTimeout(Key key, int msgId) {
    debug(LOG_TAG, "Informing observers timeout: looking for " + key.toString());

    List<ObserverEntry> obsList = this.observerMap.get(key);

    if (null == obsList) {
      debug(LOG_TAG, "No observers found for " + key.toString());
      return;
    }

    List<ObserverEntry> removableEntries = new LinkedList<ObserverEntry>();
    synchronized (removableEntries) {
      for (ObserverEntry e : obsList) {
        debug(LOG_TAG, "Observers found for " + key.toString()
            + ", delivering timeout notification");
        e.obs.onDeviceMessageTimeout(msgId);
        if (e.removeWhenNotified) {
          debug(LOG_TAG, "Observer is going to be removed from list");
          removableEntries.add(e);
        }
      }

      for (ObserverEntry e : removableEntries) {
        obsList.remove(e);
      }
    }
  }

  /**
   * Method registerObserver.
   * 
   * @param obs IObserver
   * @param address InetAddress
   * @param messageId int
   * @param oneShot boolean
   * 
   * @see org.keeper.iot.wsn.plc.observer.IBroker#registerObserver(IObserver, InetAddress, int,
   *      boolean)
   */
  @Override
  public void registerObserver(IObserver obs, InetAddress address, int messageId, boolean oneShot) {
    regObserver(obs, new Key(address, messageId), oneShot);
  }

  /**
   * Method registerObserver.
   * 
   * @param obs IObserver
   * @param messageId int
   * @param oneShot boolean
   * 
   * @see org.keeper.iot.wsn.plc.observer.IBroker#registerObserver(IObserver, int, boolean)
   */
  @Override
  public void registerObserver(IObserver obs, int messageId, boolean oneShot) {
    regObserver(obs, new Key(messageId), oneShot);
  }

  private static boolean listContainsObserver(List<ObserverEntry> obsList, IObserver obs) {
    for (ObserverEntry e : obsList) {
      if (e.obs.equals(obs)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Method registerObserver.
   * 
   * @param obs IObserver
   * @param key Key
   * @param oneshot boolean
   */
  private void regObserver(IObserver obs, Key key, boolean oneshot) {
    if (null == obs) {
      warn(LOG_TAG, "Failed to register observer. Can't be null.");
      return;
    }

    trace(LOG_TAG, "Adding new observer: " + key.toString());
    List<ObserverEntry> obsList = this.observerMap.get(key);
    if (null == obsList) {
      debug(LOG_TAG, "Creating new observer list for key: " + key.toString());
      obsList = new LinkedList<ObserverEntry>();
      this.observerMap.put(key, (LinkedList<ObserverEntry>) obsList);
    } else {
      synchronized (obsList) {
        if (listContainsObserver(obsList, obs)) {
          warn(LOG_TAG, "Can't register observer for same key more than once");
          return;
        }
      }
    }

    if (TRACE) {
      String oneShotStr = "";
      if (oneshot) {
        oneShotStr = "(oneshot) ";
      }
      trace(LOG_TAG, "Adding observer " + oneShotStr + "to existing list for " + key.toString());
    }
    synchronized (obsList) {
      obsList.add(new ObserverEntry(obs, oneshot));
    }
  }

  /**
   * Method removeObserver.
   * 
   * @param obs IObserver
   * @param address InetAddress
   * @param messageId int
   * 
   * @see org.keeper.iot.wsn.plc.observer.IBroker#removeObserver(IObserver, InetAddress, int)
   */
  @Override
  public void removeObserver(IObserver obs, InetAddress address, int messageId) {
    if (null == obs || null == address) {
      warn(LOG_TAG, "Failed to register observer. Arguments can't be null.");
      return;
    }
    remObserver(obs, new Key(address, messageId));
  }

  /**
   * Method removeObserver.
   * 
   * @param obs IObserver
   * @param messageId int
   * 
   * @see org.keeper.iot.wsn.plc.observer.IBroker#removeObserver(IObserver, int)
   */
  @Override
  public void removeObserver(IObserver obs, int messageId) {
    if (null == obs) {
      warn(LOG_TAG, "Failed to register observer. Can't be null.");
      return;
    }
    remObserver(obs, new Key(messageId));
  }

  /**
   * Method removeObserver.
   * 
   * @param obs IObserver
   * @param key Key
   */
  private void remObserver(IObserver obs, Key key) {
    List<ObserverEntry> obsList = this.observerMap.get(key);
    if (null == obsList) {
      warn(LOG_TAG, "Attempted to remove an observer that doesn't exist");
      return;
    }

    synchronized (obsList) {
      // There shouldn't be multiple entries for the same observer
      ObserverEntry rem = null;
      for (ObserverEntry e : obsList) {
        if (e.obs.equals(obs)) {
          rem = e;
          break;
        }
      }

      if (null != rem) {
        debug(LOG_TAG, "Removing observer " + rem.toString());
        obsList.remove(rem);
      } else {
        warn(LOG_TAG, "Attempted to remove an observer that isn't registered");
      }

      if (obsList.isEmpty()) {
        trace(LOG_TAG, "Observer list is empty, removing it from map.");
        this.observerMap.remove(key);
      }
    }
  }

  /**
   * Method onPlcMessage.
   * 
   * @param msg Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IPlcMessageListener#onPlcMessage(Message)
   */
  @Override
  public void onPlcMessage(Message msg) {
    informObservers(new Key(msg.getAddress(), msg.getId()), msg);
    informObservers(new Key(msg.getId()), msg);
  }

  /**
   * Method onPlcMessageTimeout.
   * 
   * @param msg Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IPlcMessageListener#onPlcMessageTimeout(Message)
   */
  @Override
  public void onPlcMessageTimeout(Message msg) {
    informObserversTimeout(new Key(msg.getAddress(), msg.getId()), msg.getResponseId());
    informObserversTimeout(new Key(msg.getId()), msg.getResponseId());
  }

}
