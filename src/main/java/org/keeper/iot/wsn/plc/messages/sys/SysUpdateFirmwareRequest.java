/**
 * SysUpdateFirmwareRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * Implements the message builder for SysUpdateFirmwareRequest.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class SysUpdateFirmwareRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysUpdateFirmwareRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_SYS,
            Sys.SYS_CMD_FW_UPDATE, 0};
    Message message = new SysUpdateFirmwareRequest(address, messageData);

    return message;
  }

  /**
   * SysUpdateFirmwareRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private SysUpdateFirmwareRequest(InetAddress address, byte[] data) {
    super(address, data, SysUpdateFirmwareResponse.REGISTRATION_ID);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + super.toString();
  }
}
