package org.keeper.iot.wsn.plc.containers;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetCapabilitiesRequest;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetCapabilitiesResponse;
import org.keeper.iot.wsn.plc.messages.nwk.Nwk;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceDeviceRequest;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceRequest;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceResponse;
import org.keeper.iot.wsn.plc.messages.nwk.NwkKeepaliveDeviceRequest;
import org.keeper.iot.wsn.plc.messages.nwk.NwkKeepaliveResponse;
import org.keeper.iot.wsn.plc.observer.IObserver;
import org.keeper.iot.wsn.plc.observer.Key;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.naming.directory.InvalidAttributesException;

public class SuperSpawner implements IObserver, Nwk, AppGenerics {
  private static final String TAG = SuperSpawner.class.getSimpleName();
  private final ICore core;
  private Map<Key, IContainer> handlers = new ConcurrentHashMap<>(16, 0.75f);
  private final IDeviceObserver endpointObserver;


  public SuperSpawner(ICore core, IDeviceObserver endpointObserver) {
    this.core = core;
    this.endpointObserver = endpointObserver;

    core.subscribe(this, NwkKeepaliveDeviceRequest.REGISTRATION_ID);
    core.subscribe(this, NwkAnnounceDeviceRequest.REGISTRATION_ID);
    core.subscribe(this, NwkAnnounceResponse.REGISTRATION_ID);
    core.subscribe(this, AppGenericsGetCapabilitiesResponse.REGISTRATION_ID);
  }

  @Override
  public void onAppGenericsGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {}

  @Override
  public void onAppGenericsGetAttributeResponse(InetAddress address, int status, byte endpoint,
      byte clusterId, byte attributeId, byte attributeDatatype, byte[] attributeData,
      byte sequenceNumber) {}

  @Override
  public void onAppGenericsSetAttributeResponse(InetAddress address, int status, byte clusterId,
      byte attributeId, byte sequenceNum) {}

  @Override
  public void onAppGenericsPeriodicReportConfigureResponse(InetAddress address, byte endpoint,
      int status, byte clusterId, byte attributeId, byte sequenceNum) {}

  @Override
  public void onAppGenericsPeriodicReport(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte[] attributeData, byte sequenceNumber) {}

  @Override
  public void onAppGenericsAlertConfigureResponse(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte sequenceNum) {}

  @Override
  public void onAppGenericsRangeAlert(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte[] attributeData, byte alertType) {}

  @Override
  public void onAppGenericsCapabilities(InetAddress address, int status,
      Capabilities capabilities) {
    if (MessageStatus.isStatusSuccessfull(TAG, status)) {
      info(TAG, "Received capabilities response from " + address.getHostAddress());
      try {
        Key key = new Key(address);
        IContainer handler = new SuperContainer(capabilities, core, address);
        handlers.put(key, handler);
        if (null != endpointObserver) {
          endpointObserver.onNewDevice(handler);
        }
        updateResources(address);
      } catch (InvalidAttributesException e) {
        error(TAG, "Error spawning handler for device " + address);
        e.printStackTrace();
      }
    } else {
      error(TAG, "Error getting capabilities from " + address.getHostAddress());
    }
  }

  @Override
  public void onNwkGetVersionResponse(InetAddress address, int status, byte version) {}

  @Override
  public void onNwkAnnounceRequest(InetAddress address) {
    info(TAG,
        "Received announce request from " + address.getHostAddress() + ". Allowing device to join");
    core.send(NwkAnnounceRequest.createMessage(address, true));

    Key key = new Key(address);
    if (!handlers.containsKey(key)) {
      info(TAG, "Requesting capabilities for " + address.getHostAddress());
      core.send(AppGenericsGetCapabilitiesRequest.createMessage(address));
    }
  }

  @Override
  public void onNwkAnnounceResponse(InetAddress address, int status) {
    info(TAG, "Received announce confirmation from " + address.getHostAddress());
  }

  @Override
  public void onNwkLeaveResponse(InetAddress address, int status) {}

  @Override
  public void onNwkKeepaliveRequest(InetAddress address) {
    info(TAG, "Received keepalive request from " + address.getHostAddress());
    core.send(NwkKeepaliveResponse.createMessage(address));

    updateResources(address);
  }

  private void updateResource(IContainer handler, String[] path) {
    PlcResource res = handler.getChild(path);
    if (null == res) {
      info(TAG, "Child came null!");
    } else {
      info(TAG, "Got child " + res.getName());
      res.remoteGet();
    }
  }

  private void setResource(IContainer handler, String[] path, List<String> parameters) {
    PlcResource res = handler.getChild(path);
    if (null == res) {
      info(TAG, "Child came null!");
    } else {
      info(TAG, "Got child " + res.getName());
      res.remoteSet(parameters);
    }
  }

  private void updateResources(InetAddress address) {
    Key key = new Key(address);
    IContainer handler = handlers.get(key);

    if (null == handler) {
      return;
    }

    // updateResource(handler, new String[] {"Endpoint5", "OnOff", "Version"});
    // updateResource(handler, new String[] {"Endpoint5", "OnOff", "State"});
    // updateResource(handler, new String[] {"Endpoint5", "OnOff", "Mode"}); // seems to be
    // unsupported
    // updateResource(handler, new String[] {"System", "Version"});
    // updateResource(handler, new String[] {"System", "Firmware", "CurrentVersion"});
    // updateResource(handler, new String[] {"System", "Date"});
    // updateResource(handler, new String[] {"System", "HardwareRevision"});
    // updateResource(handler, new String[] {"System", "ManufacturerId"});
    // updateResource(handler, new String[] {"System", "ModelId"});
    // updateResource(handler, new String[] {"System", "PowerSources"});
    // updateResource(handler, new String[] {"System", "SerialNumber"});
    // updateResource(handler, new String[] {"System", "CurrentTemperature"});
    // updateResource(handler, new String[] {"System", "UserActions"});
    // updateResource(handler, new String[] {"Network", "Version"});
    // updateResource(handler, new String[] {"Endpoint0", "Device", "Version"});
    // updateResource(handler, new String[] {"Endpoint0", "Device", "ID"});
    // updateResource(handler, new String[] {"Endpoint0", "Identify", "Version"});
    // updateResource(handler, new String[] {"Endpoint0", "Identify", "State"});

    // updateResource(handler, new String[] {"Endpoint0", "SmartTemp", "Version"});
    // updateResource(handler, new String[] {"Endpoint0", "SmartTemp", "CurrentTemperature"});
    // updateResource(handler, new String[] {"Endpoint0", "SmartTemp", "MaxTemperature"});
    // updateResource(handler, new String[] {"Endpoint0", "SmartTemp", "MinTemperature"});

    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "Version"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_E_A_C__A"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_E_A_P__A"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_E_R_P__Q1"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_E_R_P__Q2"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_E_R_N__Q3"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_E_R_N__Q4"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_C_I__ALL"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_P_R_P__Q1"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_P_R_P__Q2"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_P_R_N__Q3"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_P_R_N__Q4"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_P_A_P__ALL"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_P_A_N__ALL"});
    // updateResource(handler, new String[] {"Endpoint1", "SmartEnergy", "SE_ATT_F"});

    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "Version"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_E_A_C__AL1"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_E_A_P__AL1"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_V_I__L1"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_C_I__L1"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_P_A_P__L1"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_P_A_N__L1"});
    // updateResource(handler, new String[] {"Endpoint2", "SmartEnergy", "SE_ATT_P__PFL1"});
    //
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "Version"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_E_A_C__AL2"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_E_A_P__AL2"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_V_I__L2"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_C_I__L2"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_P_A_P__L2"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_P_A_N__L2"});
    // updateResource(handler, new String[] {"Endpoint3", "SmartEnergy", "SE_ATT_P__PFL2"});
    //
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "Version"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_E_A_C__AL3"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_E_A_P__AL3"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_V_I__L3"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_C_I__L3"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_P_A_P__L3"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_P_A_N__L3"});
    // updateResource(handler, new String[] {"Endpoint4", "SmartEnergy", "SE_ATT_P__PFL3"});

    // updateResource(handler, new String[] {"Endpoint5", "OnOff", "Version"});
    // updateResource(handler, new String[] {"Endpoint5", "OnOff", "State"});
    //
    // updateResource(handler, new String[] {"Endpoint6", "OnOff", "Version"});
    // updateResource(handler, new String[] {"Endpoint6", "OnOff", "State"});

    handler.remoteUpdateAll();

    // List<String> onOffParams = new ArrayList<String>();
    // onOffParams.add("Toggle");
    // setResource(handler, new String[] {"Endpoint5", "OnOff", "State"}, onOffParams);
    // setResource(handler, new String[] {"Endpoint6", "OnOff", "State"}, onOffParams);

    // setResource(handler, new String[] {"System", "Reboot"}, null);

    // List<String> idParams = new ArrayList<String>();
    // idParams.add("SomeCustomID ");
    // setResource(handler, new String[] {"Endpoint0", "Device", "ID"}, idParams);

    trace(TAG, handler.getRoot().toString());
  }

  @Override
  public void onNwkKeepaliveConfigureResponse(InetAddress address, int status) {}

  @Override
  public void onDeviceMessage(Message msg) {
    info(TAG, "Got message from " + msg.getAddress().getHostAddress());
    msg.callHandler(this);
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    warn(TAG, "Got message timeout for message id 0x" + Helpers.intToHexString(msgId));
  }
}
