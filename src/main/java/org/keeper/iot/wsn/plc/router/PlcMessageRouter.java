/**
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.router;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.core.Configuration;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.PlcMessageFactory;
import org.keeper.udp.IDatagramListener;
import org.keeper.udp.IDatagramSender;
import org.keeper.utils.Helpers;

import java.net.DatagramPacket;

/**
 * When a datagram is received this tries to parse it into a known message, also notifies the
 * resource holder {@link ResourceHolder}. Also allows client to enqueue messages that will be sent
 * when there is no other message being executed a specific target device.
 * 
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class PlcMessageRouter implements IDatagramListener, IRouter {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = PlcMessageRouter.class.getSimpleName();

  /**
   * Captures datagrams and delivers plc messages.
   */
  private final IPlcMessageListener listener;

  /**
   * To send datagrams.
   */
  private IDatagramSender datagramSender = null;

  /**
   * Holds messages while target device is busy.
   */
  private final IMessageQueue messageQueue = new ResourceHolder(this);

  /**
   * @param listener Listener that will receive Plc Messages.
   */
  public PlcMessageRouter(IPlcMessageListener listener) {
    trace(LOG_TAG, "constructor");
    if (listener == null) {
      throw new IllegalArgumentException("listener can't be null");
    }
    this.listener = listener;
  }

  /**
   * Method setDatagramSender.
   * 
   * @param sender IDatagramSender
   */
  public void setDatagramSender(IDatagramSender sender) {
    this.datagramSender = sender;
  }

  /**
   * Method enqueue.
   * 
   * @param message Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IRouter#enqueue(Message)
   */
  @Override
  public void enqueue(Message message) {
    if (message.getResponseId() != 0) {
      this.messageQueue.addMessage(message);
    } else {
      debug(LOG_TAG, "Message has null response id, skipping queue");
      send(message);
    }
  }

  /**
   * Method send.
   * 
   * @param msg Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IRouter#send(Message)
   */
  @Override
  public void send(Message msg) {
    if (this.datagramSender != null) {
      trace(LOG_TAG, "Sending message: " + msg.toString() + MessageDebugHelpers.printRawData(msg));
      this.datagramSender.sendPacket(new DatagramPacket(msg.getRaw(), msg.getRaw().length, msg
          .getAddress(), Configuration.PLC_ENDPOINT_PORT));
    }
  }

  /**
   * Method onDatagram.
   * 
   * @param packet DatagramPacket
   * 
   * @see org.keeper.udp.IDatagramListener#onDatagram(DatagramPacket)
   */
  @Override
  public void onDatagram(DatagramPacket packet) {
    trace(
        LOG_TAG,
        "Received new udp packet with data:\n"
            + Helpers.arrayToHexStringPretty(packet.getData(), (packet.getData().length > 25) ? 25
                : packet.getData().length) + " ...");

    Message message = PlcMessageFactory.createMessage(packet.getAddress(), packet.getData());

    if (null != message) {
      trace(LOG_TAG,
          "Message arrived: " + message.toString() + MessageDebugHelpers.printRawData(message));

      this.messageQueue.notifyMessageReceived(message);
      this.listener.onPlcMessage(message);
    } else {
      warn(LOG_TAG, "Message arrived: UNKNOWN format");
    }
  }

  /**
   * Method onTimeout.
   * 
   * @param msg Message
   * 
   * @see org.keeper.iot.wsn.plc.router.IRouter#onTimeout(Message)
   */
  @Override
  public void onTimeout(Message msg) {
    this.listener.onPlcMessageTimeout(msg);
  }

}
