/**
 * Creates a get version response {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.IResponse;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsConfigurePeriodicReportResponse extends Message implements IResponse {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsConfigurePeriodicReportResponse.class
      .getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_GENERICS,
        AppGenerics.GEN_CMD_CFG_PERIODIC_REPORT, endpoint, 0, 0});
  }

  /**
   * The plc status bitfields.
   */
  private final int status;

  /**
   * Attribute cluster id.
   */
  private final byte clusterId;

  /**
   * Attribute id.
   */
  private final byte attributeId;

  /**
   * The sequence number of this message.
   */
  private final byte sequenceNumber;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  protected AppGenericsConfigurePeriodicReportResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.clusterId = data[8];
    this.attributeId = data[9];
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 10);
    this.sequenceNumber = MessageFields.getSequenceNumber(data);
  }

  /**
   * Don't allow.
   */
  private AppGenericsConfigurePeriodicReportResponse() {
    super(null, null);
    this.status = 0;
    this.sequenceNumber = 0;
    this.clusterId = 0;
    this.attributeId = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppGenerics) {
      ((AppGenerics) obs).onAppGenericsPeriodicReportConfigureResponse(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.clusterId, this.attributeId,
          this.sequenceNumber);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
      + super.toString()
      + MessageDebugHelpers.printEndpoint(super.data)
      + MessageDebugHelpers.printStatus(this.status)
      + MessageDebugHelpers.printClusterId(clusterId)
      + MessageDebugHelpers.printSequenceNumber(super.data);
  }
}
