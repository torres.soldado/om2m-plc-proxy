/**
 * Creates a get version response {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import static com.esotericsoftware.minlog.Log.trace;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Capabilities;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Cluster;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Endpoint;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsGetCapabilitiesResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsGetCapabilitiesResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
      MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_GENERICS,
      AppGenerics.GEN_CMD_GET_CAPABILITIES, 1, 0, 0});
  
  /**
   * The plc status bit-fields.
   */
  private final int status;

  private final Capabilities capabilities;

  public Capabilities getCapabilities() {
    return capabilities;
  }

  /**
   * @param data
   * @param dataOffset
   * @return
   */
  private int fillInSubsystems(byte[] data, int dataOffset) {
    int i = dataOffset;

    int numSubsystems = data[i++];
    capabilities.subsystems = new ArrayList<Byte>(numSubsystems);
    trace(LOG_TAG, "Capabilities, num_subsystems: " + Integer.toString(numSubsystems));

    for (int j = 0; j < numSubsystems; ++j) {
      capabilities.subsystems.add(data[i++]);
      trace(
          LOG_TAG,
          "Subsystem #" + Integer.toString(j) + " id: "
              + Integer.toString(capabilities.subsystems.get(j)));
    }

    return i;
  }

  /**
   * @param data
   * @param dataOffset
   * @return
   */
  private int fillInEndpoints(byte[] data, int dataOffset) {
    int i = dataOffset;

    int numEndpoint = data[i++];
    capabilities.endpoints = new ArrayList<AppGenerics.Endpoint>(numEndpoint);
    trace(LOG_TAG, "num_endpoints: " + Integer.toString(numEndpoint));

    for (int j = 0; j < numEndpoint; ++j) {
      Endpoint endpoint = new Endpoint();
      endpoint.id = data[i++];
      capabilities.endpoints.add(endpoint);
      trace(LOG_TAG, "Endpoint #" + Integer.toString(j) + " id: " + Integer.toString(endpoint.id));
    }

    return i;
  }

  /**
   * 
   * @param data
   * @param dataOffset
   * @return
   */
  private int fillInClusters(byte[] data, int dataOffset) {
    int i = dataOffset;

    int numEndpoint = capabilities.endpoints.size();

    for (int j = 0; j < numEndpoint; ++j) {
      int endpointId = capabilities.endpoints.get(j).id;

      int numClusters = data[i++];
      capabilities.endpoints.get(j).clusters = new ArrayList<Cluster>(numClusters);
      trace(
          LOG_TAG,
          "Endpoint(" + Integer.toString(endpointId) + "), num_clusters: "
              + Integer.toString(numClusters));

      // get cluster ids
      for (int k = 0; k < numClusters; ++k) {
        Cluster cluster = new Cluster();
        cluster.id = data[i++];
        capabilities.endpoints.get(j).clusters.add(cluster);
        trace(LOG_TAG,
            "Endpoint(" + Integer.toString(endpointId) + "), cluster#" + Integer.toString(k) + "("
                + Integer.toString(cluster.id) + ")");
      }

      for (int k = 0; k < numClusters; ++k) {
        int clusterId = capabilities.endpoints.get(j).clusters.get(k).id;

        int numAttributes = data[i++];
        capabilities.endpoints.get(j).clusters.get(k).attributes =
            new ArrayList<Byte>(numAttributes);
        trace(
            LOG_TAG,
            "Endpoint(" + Integer.toString(endpointId) + "), cluster("
                + Integer.toString(clusterId) + "), num_attributes: "
                + Integer.toString(numAttributes));

        // get attribute ids
        for (int l = 0; l < numAttributes; ++l) {
          byte attributeId = data[i++];
          capabilities.endpoints.get(j).clusters.get(k).attributes.add(attributeId);
          trace(
              LOG_TAG,
              "Endpoint(" + Integer.toString(endpointId) + "), cluster("
                  + Integer.toString(clusterId) + "), attribute #" + Integer.toString(l) + " id: "
                  + Integer.toString(attributeId));
        }
      }
    }

    return i;
  }

  /**
   * Message sanity fail-fast test method.
   * 
   * @param data Raw message data.
   * @return true if message appears valid, false otherwise.
   */
  private boolean isValidCapabilitiesMessage(byte[] data) {
    int offset = 11;
    if (offset > data.length) {
      return false;
    }

    // Get num subsystems and their ids.
    byte numSubsystems = data[offset++];
    offset += numSubsystems;
    if (offset > data.length) {
      return false;
    }

    // Get num endpoints and their ids.
    byte numEndpoints = data[offset++];
    offset += numEndpoints;
    if (offset > data.length) {
      return false;
    }

    // For each endpoint get cluster info
    for (int i = 0; i < numEndpoints; ++i) {
      byte numClusters = data[offset++];
      offset += numClusters;
      if (offset > data.length) {
        return false;
      }

      // For each cluster get attribute info
      for (int j = 0; j < numClusters; ++j) {
        byte numAttributes = data[offset++];
        offset += numAttributes;
        if (offset > data.length) {
          return false;
        }
      }
    }

    int len = MessageFields.getLength(data);
    if (offset != len) {
      return false;
    }

    return true;
  }

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppGenericsGetCapabilitiesResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 6);
    capabilities = new Capabilities();
    capabilities.endpoints = new LinkedList<AppGenerics.Endpoint>();

    if (!isValidCapabilitiesMessage(data)) {
      throw new IllegalArgumentException("Message length to short");
    }

    capabilities.version = data[10];
    trace(LOG_TAG, "Capabilities, version:0x" + Helpers.byteToHexString(capabilities.version));
    fillInClusters(data, fillInEndpoints(data, fillInSubsystems(data, 11)));
  }

  /**
   * Don't allow.
   */
  private AppGenericsGetCapabilitiesResponse() {
    super(null, null);
    this.status = 0;
    this.capabilities = null;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppGenerics) {
      ((AppGenerics) obs).onAppGenericsCapabilities(super.address, this.status, this.capabilities);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + capabilities.toString();
  }
}
