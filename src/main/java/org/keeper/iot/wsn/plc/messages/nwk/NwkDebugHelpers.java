package org.keeper.iot.wsn.plc.messages.nwk;

import static com.esotericsoftware.minlog.Log.warn;

public class NwkDebugHelpers {
  public static void unhandledOnNwkGetVersionResponse(String tag) {
    warn(tag, "Unhandled onNwkGetVersionResponse message");
  }
  
  public static void unhandledOnNwkAnnounceRequest(String tag) {
    warn(tag, "Unhandled onNwkAnnounceRequest message");
  }
  
  public static void unhandledOnNwkAnnounceResponse(String tag) {
    warn(tag, "Unhandled onNwkAnnounceResponse message");
  }
  
  public static void unhandledOnNwkLeaveResponse(String tag) {
    warn(tag, "Unhandled onNwkLeaveResponse message");
  }
  
  public static void unhandledOnNwkKeepaliveRequest(String tag) {
    warn(tag, "Unhandled onNwkKeepaliveRequest message");
  }
  
  public static void unhandledOnNwkKeepaliveConfigureResponse(String tag) {
    warn(tag, "Unhandled onNwkGetVersionResponse message");
  }
}
