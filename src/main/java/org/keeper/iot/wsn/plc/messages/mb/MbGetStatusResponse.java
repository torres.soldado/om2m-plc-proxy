/**
 * Creates a modbus get status response message {@link Message} from raw data.
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.mb;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class MbGetStatusResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = MbGetStatusResponse.class.getSimpleName();
  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields
      .createId(new byte[] {0, 0, MessageFields.SOF,
          MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_MB, Mb.MB_GET_STATUS, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public MbGetStatusResponse(InetAddress address, byte[] data) throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
  }

  /**
   * Don't allow default ctor.
   */
  private MbGetStatusResponse() {
    super(null, null);
    this.status = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Mb) {
      ((Mb) obs).onMbGetStatusResponse(super.getAddress(), this.status);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + super.toString() + ", Status("
        + MessageStatus.getStatusStr(this.status) + ")";
  }
}
