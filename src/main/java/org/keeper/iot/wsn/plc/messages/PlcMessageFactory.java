/**
 * Plc message factory. Parses and creates a Plc message from device addresss and raw data.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages;

import static com.esotericsoftware.minlog.Log.TRACE;
import static com.esotericsoftware.minlog.Log.error;
import static com.esotericsoftware.minlog.Log.trace;
import static com.esotericsoftware.minlog.Log.warn;

import org.keeper.iot.wsn.plc.messages.appdevice.AppDevice;
import org.keeper.iot.wsn.plc.messages.appdevice.AppDeviceGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeResponse;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetCapabilitiesResponse;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsPeriodicReport;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsRangeAlert;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsSetAttributeResponse;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentify;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifyGetStateResponse;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifyGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.appidentify.AppIdentifySetIdentifyResponse;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOff;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffSetRelayOffResponse;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffSetRelayOnResponse;
import org.keeper.iot.wsn.plc.messages.apponoff.AppOnOffSetRelayToggleResponse;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyCommands;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyGetMeterModeResponse;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergyGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.appsmartenergy.AppSmartEnergySetMeterModeResponse;
import org.keeper.iot.wsn.plc.messages.appsmarttemp.AppSmartTempGetCurTempResponse;
import org.keeper.iot.wsn.plc.messages.appsmarttemp.AppSmartTempGetMaxTempResponse;
import org.keeper.iot.wsn.plc.messages.appsmarttemp.AppSmartTempGetMinTempResponse;
import org.keeper.iot.wsn.plc.messages.appsmarttemp.AppSmarttemp;
import org.keeper.iot.wsn.plc.messages.appsmarttemp.AppSmartTempGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.dbg.Dbg;
import org.keeper.iot.wsn.plc.messages.dbg.DbgGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.dummy.DummyResponseMessage;
import org.keeper.iot.wsn.plc.messages.mb.Mb;
import org.keeper.iot.wsn.plc.messages.mb.MbGetStatusResponse;
import org.keeper.iot.wsn.plc.messages.mb.MbGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.mb.MbReceiveDataFromDevice;
import org.keeper.iot.wsn.plc.messages.mb.MbSendDataResponse;
import org.keeper.iot.wsn.plc.messages.mb.MbSetEnableResponse;
import org.keeper.iot.wsn.plc.messages.nwk.Nwk;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceDeviceRequest;
import org.keeper.iot.wsn.plc.messages.nwk.NwkAnnounceResponse;
import org.keeper.iot.wsn.plc.messages.nwk.NwkDeviceLeaveResponse;
import org.keeper.iot.wsn.plc.messages.nwk.NwkGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.nwk.NwkKeepaliveConfigurationResponse;
import org.keeper.iot.wsn.plc.messages.nwk.NwkKeepaliveDeviceRequest;
import org.keeper.iot.wsn.plc.messages.sys.Sys;
import org.keeper.iot.wsn.plc.messages.sys.SysFactoryResetResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysFirmwareChunkUploadResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetDateResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetFirmwareVersionResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetHardwareRevisionResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetManufacturerIdResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetModelIdResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetPowersourceResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetSerialResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetTemperatureResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysGetVersionResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysRebootResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysSetDateResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysUpdateFirmwareResponse;
import org.keeper.iot.wsn.plc.messages.sys.SysUserDidSomethingNotification;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.security.InvalidParameterException;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 * @BUG OnOff version request PM responds with SREQ instead of SRSP.
 */
public final class PlcMessageFactory {
  /**
   * Used by logger.
   */
  public static final String LOG_TAG = PlcMessageFactory.class.getSimpleName();

  /**
   * String representation.
   * 
   * @return String representation. * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "";
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createAReqAppMessage(InetAddress address, byte[] data) {
    final byte clusterId = MessageFields.getCmd1AkaClusterId(data);
    final byte clusterCmdId;

    if (data.length < 7) {
      warn(LOG_TAG, "Message too short to be APP");
      return null;
    }

    clusterCmdId = MessageFields.getClusterCmdId(data);
    if (clusterId == MessageFields.APP_CLUSTER_GENERICS) {
      switch (clusterCmdId) {
        case AppGenerics.GEN_CMD_PERIODIC_REPORT:
          return new AppGenericsPeriodicReport(address, data);
        case AppGenerics.GEN_CMD_RANGE_ALERT:
          return new AppGenericsRangeAlert(address, data);
        default:
          return null;
      }
    } else {
      return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createAReqMessage(InetAddress address, byte[] data) {
    final byte cmd0 = MessageFields.getCmd0(data);
    final byte system = MessageFields.getSystem(cmd0);
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);

    switch (system) {
      case MessageFields.SUBSYSTEM_APP:
        return createAReqAppMessage(address, data);
      case MessageFields.SUBSYSTEM_MB:
        if (cmd1 == Mb.MB_COM_DATA) {
          return new MbReceiveDataFromDevice(address, data);
        }
        return null;
      case MessageFields.SUBSYSTEM_NWK:
        if (cmd1 == Nwk.NWK_ANNOUNCE) {

          return new NwkAnnounceDeviceRequest(address, data);
        }
        return null;
      case MessageFields.SUBSYSTEM_SYS:
        if (cmd1 == Sys.SYS_CMD_LOCAL_ACTION) {
          return new SysUserDidSomethingNotification(address, data);
        }
        return null;
      default:
        return null;
    }
  }

  private static Message createSReqNwkMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);

    if (cmd1 == Nwk.NWK_KEEPALIVE) {
      return new NwkKeepaliveDeviceRequest(address, data);
    }

    return null;
  }

  /**
   * @BUG Because this response should be an SRSP.
   * @param address
   * @param data
   * @return
   */
  private static Message createSReqAppDevMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);

    if (clusterCmdId == AppDevice.DEV_CMD_GET_VERSION) {
      return new AppDeviceGetVersionResponse(address, data);
    }

    return null;
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSReqAppMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);
    final byte clusterId = cmd1;

    if (data.length < 7) {
      warn(LOG_TAG, "Message too short to be APP");
      return null;
    }

    switch (clusterId) {
      case MessageFields.APP_CLUSTER_DEVICE:
        return createSReqAppDevMessage(address, data);
        // case MessageFields.APP_CLUSTER_GENERICS:
        // return createSRspAppGenericsMessage(address, data);
        // case MessageFields.APP_CLUSTER_IDENTIFY:
        // return createSRspAppIdentifyMessage(address, data);
      case MessageFields.APP_CLUSTER_ONOFF:
        return createSReqAppOnOffMessage(address, data);
        // case MessageFields.APP_CLUSTER_SMARTENERGY:
        // return createSRspAppSmartEnergyMessage(address, data);
        // case MessageFields.APP_CLUSTER_SMARTTEMP:
        // return createSRspAppSmartTempMessage(address, data);
      default:
        return null;
    }
  }

  private static Message createSReqAppOnOffMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);

    switch (clusterCmdId) {
      case AppOnOff.ONOFF_CMD_GET_VERSION:
        return new AppOnOffGetVersionResponse(address, data);
        // case AppOnOff.ONOFF_CMD_ON:
        // return new AppOnOffSetRelayOnResponse(address, data);
        // case AppOnOff.ONOFF_CMD_OFF:
        // return new AppOnOffSetRelayOffResponse(address, data);
        // case AppOnOff.ONOFF_CMD_TOGGLE:
        // return new AppOnOffSetRelayToggleResponse(address, data);
      default:
        return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSReqMessage(InetAddress address, byte[] data) {
    final byte cmd0 = MessageFields.getCmd0(data);
    final byte system = MessageFields.getSystem(cmd0);
    // final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);

    switch (system) {
      case MessageFields.SUBSYSTEM_APP:
        return createSReqAppMessage(address, data);
        // case MessageFields.SUBSYSTEM_DBG:
        // return createSRspDbgMessage(address, data);
        // case MessageFields.SUBSYSTEM_MB:
        // return createSRspMbMessage(address, data);
      case MessageFields.SUBSYSTEM_NWK:
        return createSReqNwkMessage(address, data);
        // case MessageFields.SUBSYSTEM_SYS:
        // return createSRspSysMessage(address, data);
      default:
        return null;
    }
  }

  private static Message createSRspAppDevMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);

    if (clusterCmdId == AppDevice.DEV_CMD_GET_VERSION) {
      return new AppDeviceGetVersionResponse(address, data);
    }

    return null;
  }

  private static Message createSRspAppGenericsMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);
    switch (clusterCmdId) {
      case AppGenerics.GEN_CMD_GET_VERSION:
        return new AppGenericsGetVersionResponse(address, data);
      case AppGenerics.GEN_CMD_READ:
        return new AppGenericsGetAttributeResponse(address, data);
      case AppGenerics.GEN_CMD_WRITE:
        return new AppGenericsSetAttributeResponse(address, data);
      case AppGenerics.GEN_CMD_CFG_PERIODIC_REPORT:
        return new AppGenericsPeriodicReport(address, data);
      case AppGenerics.GEN_CMD_CFG_RANGE_ALERT:
        return new AppGenericsRangeAlert(address, data);
      case AppGenerics.GEN_CMD_GET_CAPABILITIES:
        return new AppGenericsGetCapabilitiesResponse(address, data);
      default:
        return null;
    }
  }

  private static Message createSRspAppIdentifyMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);

    switch (clusterCmdId) {
      case AppIdentify.IDENT_CMD_GET_VERSION:
        return new AppIdentifyGetVersionResponse(address, data);
      case AppIdentify.IDENT_CMD_IDENTIFY:
        return new AppIdentifySetIdentifyResponse(address, data);
      case AppIdentify.IDENT_CMD_GET_STATE:
        return new AppIdentifyGetStateResponse(address, data);
      default:
        return null;
    }
  }

  private static Message createSRspAppOnOffMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);

    switch (clusterCmdId) {
      case AppOnOff.ONOFF_CMD_GET_VERSION:
        return new AppOnOffGetVersionResponse(address, data);
      case AppOnOff.ONOFF_CMD_ON:
        return new AppOnOffSetRelayOnResponse(address, data);
      case AppOnOff.ONOFF_CMD_OFF:
        return new AppOnOffSetRelayOffResponse(address, data);
      case AppOnOff.ONOFF_CMD_TOGGLE:
        return new AppOnOffSetRelayToggleResponse(address, data);
      default:
        return null;
    }
  }

  private static Message createSRspAppSmartEnergyMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);
    switch (clusterCmdId) {
      case AppSmartEnergyCommands.SE_CMD_GET_VERSION:
        return new AppSmartEnergyGetVersionResponse(address, data);
      case AppSmartEnergyCommands.SE_CMD_GET_OP_MODE:
        return new AppSmartEnergyGetMeterModeResponse(address, data);
      case AppSmartEnergyCommands.SE_CMD_SET_OP_MODE:
        return new AppSmartEnergySetMeterModeResponse(address, data);
      default:
        return null;
    }
  }

  private static Message createSRspAppSmartTempMessage(InetAddress address, byte[] data) {
    final byte clusterCmdId = MessageFields.getClusterCmdId(data);
    switch (clusterCmdId) {
      case AppSmarttemp.ST_CMD_GET_VERSION:
        return new AppSmartTempGetVersionResponse(address, data);
      case AppSmarttemp.ST_CMD_GET_CUR_TEMP:
        return new AppSmartTempGetCurTempResponse(address, data);
      case AppSmarttemp.ST_CMD_GET_MAX_TEMP:
        return new AppSmartTempGetMaxTempResponse(address, data);
      case AppSmarttemp.ST_CMD_GET_MIN_TEMP:
        return new AppSmartTempGetMinTempResponse(address, data);
      default:
        return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSRspAppMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);
    final byte clusterId = cmd1;

    if (data.length < 7) {
      warn(LOG_TAG, "Message too short to be APP");
      return null;
    }

    switch (clusterId) {
      case MessageFields.APP_CLUSTER_DEVICE:
        return createSRspAppDevMessage(address, data);
      case MessageFields.APP_CLUSTER_GENERICS:
        return createSRspAppGenericsMessage(address, data);
      case MessageFields.APP_CLUSTER_IDENTIFY:
        return createSRspAppIdentifyMessage(address, data);
      case MessageFields.APP_CLUSTER_ONOFF:
        return createSRspAppOnOffMessage(address, data);
      case MessageFields.APP_CLUSTER_SMARTENERGY:
        return createSRspAppSmartEnergyMessage(address, data);
      case MessageFields.APP_CLUSTER_SMARTTEMP:
        return createSRspAppSmartTempMessage(address, data);
      default:
        return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSRspDbgMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);
    if (cmd1 == Dbg.DBG_GET_VERSION) {
      return new DbgGetVersionResponse(address, data);
    } else {
      return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSRspMbMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);
    switch (cmd1) {
      case Mb.MB_GET_VERSION:
        return new MbGetVersionResponse(address, data);
      case Mb.MB_ENABLE:
        return new MbSetEnableResponse(address, data);
      case Mb.MB_COM_DATA:
        return new MbSendDataResponse(address, data);
      case Mb.MB_GET_STATUS:
        return new MbGetStatusResponse(address, data);
      default:
        return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSRspNwkMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);
    switch (cmd1) {
      case Nwk.NWK_GET_VERSION:
        return new NwkGetVersionResponse(address, data);
      case Nwk.NWK_ANNOUNCE:
        return new NwkAnnounceResponse(address, data);
      case Nwk.NWK_LEAVE:
        return new NwkDeviceLeaveResponse(address, data);
      case Nwk.NWK_KEEPALIVE_CFG:
        return new NwkKeepaliveConfigurationResponse(address, data);
      case Nwk.NWK_INVALID: // for testing
        return DummyResponseMessage.create(DummyResponseMessage.dummyAddress);
      default:
        return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSRspSysMessage(InetAddress address, byte[] data) {
    final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);

    switch (cmd1) {
      case Sys.SYS_CMD_GET_VERSION:
        return new SysGetVersionResponse(address, data);
      case Sys.SYS_CMD_GET_MANUF_ID:
        return new SysGetManufacturerIdResponse(address, data);
      case Sys.SYS_CMD_GET_MODEL:
        return new SysGetModelIdResponse(address, data);
      case Sys.SYS_CMD_GET_HW_VER:
        return new SysGetHardwareRevisionResponse(address, data);
      case Sys.SYS_CMD_GET_SERIAL:
        return new SysGetSerialResponse(address, data);
      case Sys.SYS_CMD_GET_FW_VER:
        return new SysGetFirmwareVersionResponse(address, data);
      case Sys.SYS_CMD_GET_DATE:
        return new SysGetDateResponse(address, data);
      case Sys.SYS_CMD_SET_DATE:
        return new SysSetDateResponse(address, data);
      case Sys.SYS_CMD_FW_UPDATE:
        return new SysUpdateFirmwareResponse(address, data);
      case Sys.SYS_CMD_REBOOT:
        return new SysRebootResponse(address, data);
      case Sys.SYS_CMD_GET_TEMP:
        return new SysGetTemperatureResponse(address, data);
      case Sys.SYS_CMD_GET_PWRSRC:
        return new SysGetPowersourceResponse(address, data);
      case Sys.SYS_CMD_RST_FACTORY:
        return new SysFactoryResetResponse(address, data);
      case Sys.SYS_CMD_FW_UPLOAD_CHUNK:
        return new SysFirmwareChunkUploadResponse(address, data);
      default:
        return null;
    }
  }

  /**
   * Parse raw data to create {@link Message}.
   * 
   * @param address Source device address.
   * @param data Message raw data.
   * 
   * @return Message subtype if match is found, null otherwise.
   */
  private static Message createSRspMessage(InetAddress address, byte[] data) {
    final byte cmd0 = MessageFields.getCmd0(data);
    final byte system = MessageFields.getSystem(cmd0);

    switch (system) {
      case MessageFields.SUBSYSTEM_APP:
        return createSRspAppMessage(address, data);
      case MessageFields.SUBSYSTEM_DBG:
        return createSRspDbgMessage(address, data);
      case MessageFields.SUBSYSTEM_MB:
        return createSRspMbMessage(address, data);
      case MessageFields.SUBSYSTEM_NWK:
        return createSRspNwkMessage(address, data);
      case MessageFields.SUBSYSTEM_SYS:
        return createSRspSysMessage(address, data);
      default:
        return null;
    }
  }

  /**
   * Creates a Plc {@link Message} from device address and raw data.
   * 
   * @param address Address of origin/destination device.
   * @param data Raw message data.
   * 
   *        * @return A {@link Message} if data can be parsed, null otherwise.
   */
  public static Message createMessage(InetAddress address, byte[] data) {
    if (data.length < 4) {
      warn(LOG_TAG, "Message to short");
      return null;
    }

    final byte cmd0 = MessageFields.getCmd0(data);
    final byte type = MessageFields.getType(cmd0);
    if (TRACE) {
      final byte system = MessageFields.getSystem(cmd0);
      final byte cmd1 = MessageFields.getCmd1AkaClusterId(data);

      trace(LOG_TAG, "Parsing raw message: " + MessageFields.getCmdTypeStr(type) + ", "
          + MessageFields.getSystemStr(system) + ", cmd1(0x" + Helpers.byteToHexString(cmd1) + ")");
      // + " data:\n" + Helpers.arrayToHexStringPretty(data, MessageFields.getLength(data) + 2));
    }

    try {
      switch (type) {
        case MessageFields.CMDO_AREQ:
          return createAReqMessage(address, data);
        case MessageFields.CMDO_SREQ:
          return createSReqMessage(address, data);
        case MessageFields.CMDO_SRSP:
          return createSRspMessage(address, data);
        default:
          return null;
      }
    } catch (InvalidParameterException e) {
      error(LOG_TAG, "Error parsing raw message" + e.getMessage());
      return null;
    }
  }
}
