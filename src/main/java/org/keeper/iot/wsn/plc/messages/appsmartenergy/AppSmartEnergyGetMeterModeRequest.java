/**
 * Creates a request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appsmartenergy;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.SequenceNumber;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppSmartEnergyGetMeterModeRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppSmartEnergyGetMeterModeRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte endpoint) {
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP,
            MessageFields.APP_CLUSTER_SMARTENERGY, AppSmartEnergyCommands.SE_CMD_GET_OP_MODE,
            endpoint, (byte) SequenceNumber.getSequenceNumber(), 0};
    Message message =
        new AppSmartEnergyGetMeterModeRequest(address, messageData,
            AppSmartEnergyGetVersionResponse.getRegistrationId(endpoint));

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppSmartEnergyGetMeterModeRequest(InetAddress address, byte[] data, int responseId) {
    super(address, data, responseId);
  }

  /**
   * Description of message.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printCmdClusterId(super.data)
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printSequenceNumber(super.data);
  }
}
