/**
 * Creates a get version response {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsGetVersionResponse extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsGetVersionResponse.class.getSimpleName();

  /**
   * Used to register observer against this message Id.
   * 
   * @param endpoint Endpoint.
   * 
   * @return Registration id of a AppGenericsGetVersionResponse..
   */
  public static final int getRegistrationId(byte endpoint) {
    return MessageFields.createId(new byte[] {0, 0, MessageFields.SOF,
        MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_APP, MessageFields.APP_CLUSTER_GENERICS,
        AppGenerics.GEN_CMD_GET_VERSION, endpoint, 0, 0});
  }

  /**
   * The plc status bitfields.
   */
  private final int status;
  
  /**
   * The version of this cluster.
   */
  private final byte version;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public AppGenericsGetVersionResponse(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 7);
    this.version = data[11];
  }

  /**
   * Don't allow.
   */
  private AppGenericsGetVersionResponse() {
    super(null, null);
    this.status = 0;
    this.version = 0;
  }

  /**
   * Calls the correct handler, the object must implement the Sys interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof AppGenerics) {
      ((AppGenerics) obs).onAppGenericsGetVersionResponse(super.address,
          MessageFields.getEndpoint(super.data), this.status, this.version,
          MessageFields.getSequenceNumber(super.data));
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printStatus(this.status) 
        + " Version(" + Helpers.byteToHexString(this.version) + ") "
        + MessageDebugHelpers.printSequenceNumber(super.data);
  }
}
