package org.keeper.iot.wsn.plc.core;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;

public interface ICore {
  /**
   * Method subscribe.
   * 
   * @param obs IObserver
   * @param addr InetAddress
   * @param msgId int
   */
  public void subscribe(IObserver obs, InetAddress addr, int msgId);
  
  /**
   * Method subscribe.
   * 
   * @param obs IObserver
   * @param msgId int
   */
  public void subscribe(IObserver obs, int msgId);
  
  /**
   * Method unsubscribe.
   * 
   * @param obs IObserver
   * @param addr InetAddress
   * @param msgId int
   */
  public void unsubscribe(IObserver obs, InetAddress addr, int msgId);
  
  /**
   * Method unsubscribe.
   * 
   * @param obs IObserver
   * @param msgId int
   */
  public void unsubscribe(IObserver obs, int msgId);
  
  /**
   * Method send.
   * 
   * @param msg Message
   */
  public void send(Message msg);
  
  /**
   * Method sendAndSubscribeOneShot.
   * 
   * @param obs IObserver
   * @param msg Message
   */
  public void sendAndSubscribeOneShot(IObserver obs, Message msg);
}
