package org.keeper.iot.wsn.plc.router;

import org.keeper.iot.wsn.plc.messages.Message;

/**
 * Plc Message listener interface.
 */
public interface IPlcMessageListener {
  /**
   * Method onPlcMessage.
   * @param msg Message
   */
  public void onPlcMessage(Message msg);

  /**
   * Method onPlcMessageTimeout.
   * @param msg Message
   */
  public void onPlcMessageTimeout(Message msg);
}
