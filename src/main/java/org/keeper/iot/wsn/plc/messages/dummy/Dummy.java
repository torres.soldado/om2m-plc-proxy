package org.keeper.iot.wsn.plc.messages.dummy;

import java.net.InetAddress;

public interface Dummy {
  
  public void onDummyResponse(InetAddress address);
}
