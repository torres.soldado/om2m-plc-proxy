package org.keeper.iot.wsn.plc.containers;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Capabilities;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Cluster;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenerics.Endpoint;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.InvalidAttributesException;

public class SuperContainer implements IContainer {
  /**
   * Used by logger.
   */
  private final String tag;
  /**
   * Device's network address
   */
  private final InetAddress address;

  /**
   * The root resource for this device, the name should be unique e.g. the mac address.
   */
  private final PlcResource root;

  /**
   * The list of containers/handlers for this device @see {@link org.keeper.iot.wsn.containers}
   */
  private final List<IContainer> containers = new ArrayList<IContainer>();

  /**
   * Takes a device's capabilities and dynamically builds a "collection" of containers that
   * are capable of requesting and sending data to the device. The device is then seen as a
   * tree of nodes of type {@link PlcResource}.
   * @param capabilities The capabilities structure/class of a device.
   * @param core Instance to core/engine.
   * @param address Device network (ipv4) address.
   * @throws InvalidAttributesException
   */
  public SuperContainer(Capabilities capabilities, ICore core, InetAddress address)
      throws InvalidAttributesException {
    tag = getClass().getSimpleName() + "@" + address.getHostName();
    this.address = address;
    root = new PlcResource(address.getHostAddress());

    for (Byte subsystem : capabilities.subsystems) {
      IContainer handler = null;
      switch (subsystem) {
        case MessageFields.SUBSYSTEM_SYS:
          handler = new ContainerSystem(core, address);
          break;
        case MessageFields.SUBSYSTEM_NWK:
          handler = new ContainerNetwork(core, address);
          break;
        case MessageFields.SUBSYSTEM_APP:
          break;
        default:
          warn(tag, "Unknown subsystem " + String.valueOf(subsystem));
          break;
      }
      if (null != handler) {
        containers.add(handler);
        root.addChild(handler.getRoot());
      }
    }

    for (Endpoint endpoint : capabilities.endpoints) {
      trace(tag, "Parsing endpoint: " + String.valueOf(endpoint.id) + " " + endpoint);
      PlcResource child = new PlcResource("Endpoint" + String.valueOf(endpoint.id));
      root.addChild(child);

      for (Cluster cluster : endpoint.clusters) {
        IContainer handler = null;
        switch (cluster.id) {
          case MessageFields.APP_CLUSTER_DEVICE:
            handler = new ContainerAppDevice(core, address, endpoint.id);
            break;
          case MessageFields.APP_CLUSTER_ONOFF:
            handler = new ContainerAppOnOff(core, address, endpoint.id);
            break;
          case MessageFields.APP_CLUSTER_SMARTENERGY:
            handler = new ContainerAppSmartEnergy(core, address, endpoint.id, cluster);
            break;
          case MessageFields.APP_CLUSTER_IDENTIFY:
            handler = new ContainerAppIdentify(core, address, endpoint.id);
            break;
          case MessageFields.APP_CLUSTER_SMARTTEMP:
            handler = new ContainerAppSmartTemp(core, address, endpoint.id);
            break;
          default:
            warn(tag, "Unknown cluster id " + String.valueOf(cluster.id));
            break;
        }

        if (null != handler) {
          containers.add(handler);
          child.addChild(handler.getRoot());
        }
      }
    }

    trace(tag, root.toString());
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return root.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return root;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(tag, path, parameters, root);
  }

  @Override
  public void remoteUpdateAll() {
    root.remoteGetAll();
  }

}
