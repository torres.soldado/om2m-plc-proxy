/**
 * Some helpers to get/set common message fields.
 * 
 * @author sergio.soldado@withus.pt.
 */

package org.keeper.iot.wsn.plc.messages;

import java.security.InvalidParameterException;

/**
 * @author sergio.soldado@withus.pt
 * @version $Revision: 1.0 $
 */
public final class MessageFields {

  /**
   * Disable.
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "";
  }

  /**
   * Disable.
   * 
   * @see java.lang.Object#clone()
   */
  @Override
  public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException("Not allowed");
  }

  /**
   * Cluster identifiers.
   */
  public static final byte APP_CLUSTER_GENERICS = 0;
  /**
   * Field APP_CLUSTER_DEVICE. (value is 1)
   */
  public static final byte APP_CLUSTER_DEVICE = 1;
  /**
   * Field APP_CLUSTER_ONOFF. (value is 2)
   */
  public static final byte APP_CLUSTER_ONOFF = 2;
  /**
   * Field APP_CLUSTER_SMARTENERGY. (value is 3)
   */
  public static final byte APP_CLUSTER_SMARTENERGY = 3;
  /**
   * Field APP_CLUSTER_IDENTIFY. (value is 4)
   */
  public static final byte APP_CLUSTER_IDENTIFY = 4;
  /**
   * Field APP_CLUSTER_SMARTTEMP. (value is 5)
   */
  public static final byte APP_CLUSTER_SMARTTEMP = 5;
  /**
   * Field APP_CLUSTER_INVALID. (value is 6)
   */
  public static final byte APP_CLUSTER_INVALID = 6;


  /**
   * Command type identifiers.
   */
  public static final byte CMDO_AREQ = 0x40;
  /**
   * Field CMDO_SREQ.
   */
  public static final byte CMDO_SREQ = 0x20;
  /**
   * Field CMDO_SRSP.
   */
  public static final byte CMDO_SRSP = 0x60;

  /**
   * Message max and min lengths.
   */
  public static final int MAX_MESSAGE_LENGTH = 1400;
  /**
   * Field MIN_MESSAGE_LENGTH. (value is 4)
   */
  public static final int MIN_MESSAGE_LENGTH = 4;

  /**
   * Start of frame.
   */
  public static final byte SOF = (byte) 0xFE;

  /**
   * Subsystem identifiers.
   */
  public static final byte SUBSYSTEM_SYS = 0x01;
  /**
   * Field SUBSYSTEM_NWK.
   */
  public static final byte SUBSYSTEM_NWK = 0x02;
  /**
   * Field SUBSYSTEM_DBG.
   */
  public static final byte SUBSYSTEM_DBG = 0x03;
  /**
   * Field SUBSYSTEM_MB.
   */
  public static final byte SUBSYSTEM_MB = 0x04;
  /**
   * Field SUBSYSTEM_APP.
   */
  public static final byte SUBSYSTEM_APP = 0x05;

  /**
   * The id is created differently if the message is an App system message or otherwise. An App
   * system message has the following structure: | len(2) | SOF(1) | cmd0(1) | cluster id(1) |
   * cluster command(1) | endpoint | ... The id is formed as | cmd0(1) | cluster id(1) | cluster
   * command(1) | endpoint |. Otherwise the message has the following structure: | len(2) | SOF(1) |
   * cmd0(1) | cmd1(1) | ... And the id is formed as | cmd0(1) | cmd1(1) | 0x1F | 0x1F |.
   * 
   * @param data byte[]
   * 
   * @return int
   */
  public static int createId(byte[] data) {
    final byte cmd0 = getCmd0(data);
    final byte cmd1 = getCmd1AkaClusterId(data);
    final byte system = getSystem(cmd0);
    if (system == SUBSYSTEM_APP) {
      if (data.length < 7) {
        throw new InvalidParameterException("Message is too short to be App type");
      }

      return ((cmd0 << 24) & 0xFF000000) | ((cmd1 << 16) & 0xFF0000)
          | ((getClusterCmdId(data) << 8) & 0xFF00) | (getEndpoint(data) & 0xFF);
    } else {
      return ((cmd0 << 24) & 0xFF000000) | ((cmd1 << 16) & 0xFF0000) | 0x0F00 | 0x0F;
    }
  }

  /**
   * Sets Start-of-Frame field in message.
   * 
   * @param data Message raw data.
   */
  public static void setSof(byte[] data) {
    data[2] = SOF;
  }

  /**
   * Method getCmd0.
   * 
   * @param data byte[]
   * 
   * @return byte
   */
  public static byte getCmd0(byte[] data) {
    return data[3];
  }

  /**
   * Method setCmd0.
   * 
   * @param data byte[]
   * @param cmd0 byte CMD0
   */
  public static void setCmd0(byte[] data, byte cmd0) {
    data[3] = cmd0;
  }

  /**
   * Method getCmd1AkaClusterId.
   * 
   * @param data byte[]
   * 
   * @return byte
   */
  public static byte getCmd1AkaClusterId(byte[] data) {
    return data[4];
  }

  /**
   * Method getCmd1AkaClusterId.
   * 
   * @param data byte[]
   * @param cmd1AkaClusterId byte CMD1 or Cluster Id if an APP subsystem message.
   */
  public static void setCmd1AkaClusterId(byte[] data, byte cmd1AkaClusterId) {
    data[4] = cmd1AkaClusterId;
  }

  /**
   * Method getSystem.
   * 
   * @param cmd0 byte
   * 
   * @return byte
   */
  public static byte getSystem(byte cmd0) {
    return (byte) (cmd0 & 0x1F);
  }

  /**
   * Given a message subsystem, returns it's textual representation.
   * 
   * @param subsystem The message's subsystem, see {@link Message#getSystem(byte)}
   * 
   * 
   * @return String with textual representation of the subsystem field.
   */
  public static String getSystemStr(byte subsystem) {
    switch (subsystem) {
      case SUBSYSTEM_SYS:
        return "SYS";
      case SUBSYSTEM_NWK:
        return "NWK";
      case SUBSYSTEM_DBG:
        return "DBG";
      case SUBSYSTEM_MB:
        return "MB";
      case SUBSYSTEM_APP:
        return "APP";
      default:
        return "INVALID";
    }
  }

  /**
   * Method getType.
   * 
   * @param cmd0 byte Cmd0 of message.
   * 
   * @return byte Type of message.
   */
  public static byte getType(byte cmd0) {
    return (byte) (cmd0 & 0xE0);
  }

  /**
   * Given the message type, returns a string with the type name.
   * 
   * @param type Message type, see {@link Message#getType(byte)}.
   * 
   * 
   * @return String with type name.
   */
  public static String getCmdTypeStr(byte type) {
    switch (type) {
      case CMDO_SREQ:
        return "SREQ";
      case CMDO_AREQ:
        return "AREQ";
      case CMDO_SRSP:
        return "SRSP";
      default:
        return "INVALID";
    }
  }

  /**
   * Sets cluster command, message MUST be of APP subsystem.
   * 
   * @param data Message raw data.
   * @param clusterCmdId Cluster command id.
   */
  public static void setClusterCmdId(byte[] data, byte clusterCmdId) {
    data[5] = clusterCmdId;
  }

  /**
   * Returns cluster command, message MUST be of APP subsystem.
   * 
   * @param data Message raw data.
   * 
   * 
   * @return Cluster command.
   */
  public static byte getClusterCmdId(byte[] data) {
    return data[5];
  }

  /**
   * Sets endpoint, message MUST be of APP subsystem.
   * 
   * @param data Message raw data.
   * @param endpoint Target endpoint.
   */
  public static void setEndpoint(byte[] data, byte endpoint) {
    data[6] = endpoint;
  }

  /**
   * Returns endpoint, message MUST be of APP subsystem.
   * 
   * @param data Message raw data.
   * 
   * @return Returns endpoint addressed to in the message..
   */
  public static byte getEndpoint(byte[] data) {
    return data[6];
  }

  /**
   * Returns sequence number, message MUST be of APP subsystem.
   * 
   * @param data Message raw data.
   * 
   * @return Returns endpoint addressed to in the message..
   */
  public static byte getSequenceNumber(byte[] data) {
    return data[data.length - 2];
  }

  /**
   * Method isAreq.
   * 
   * @param cmd0 byte
   * 
   * @return boolean
   */
  public static boolean isAreq(byte cmd0) {
    return getType(cmd0) == MessageFields.CMDO_AREQ;
  }

  /**
   * Method isSreq.
   * 
   * @param cmd0 byte
   * 
   * @return boolean
   */
  public static boolean isSreq(byte cmd0) {
    return getType(cmd0) == MessageFields.CMDO_SREQ;
  }

  /**
   * Method isSrsp.
   * 
   * @param cmd0 byte
   * 
   * @return boolean
   */
  public static boolean isSrsp(byte cmd0) {
    return getType(cmd0) == MessageFields.CMDO_SRSP;
  }

  /**
   * Method getLength.
   * 
   * @param messageData byte[]
   * 
   * @return int
   */
  public static int getLength(byte[] messageData) {
    return (messageData[0] & 0xFF) | ((messageData[1] & 0xFF) << 8);
  }

  /**
   * Method setChecksum.
   * 
   * @param checksum byte Checksum to set.
   * @param data byte[] Data of message.
   */
  public static void setChecksum(byte checksum, byte[] data) {
    data[getLength(data) + 1] = checksum;
  }
  
  /**
   * Calculates checksum and sets message fields.
   * 
   * @param data byte[] Raw message data.
   */
  public static void setChecksumAuto(byte[] data) {
    final byte checksum = calculateChecksum(data);
    setChecksum(checksum, data);
  }

  /**
   * Set message length fields.
   * 
   * @param len int Message length.
   * @param data byte[] Raw message data.
   */
  public static void setLen(int len, byte[] data) {
    data[0] = (byte) (len & 0xff);
    data[1] = (byte) (len >> 8);
  }

  /**
   * Calculates length and sets corresponding message fields. The message buffer shouldn't have any
   * sort of padding.
   * 
   * @param data byte[]
   */
  public static void setLenAuto(byte[] data) {
    final int len = data.length - 2;
    data[0] = (byte) (len & 0xff);
    data[1] = (byte) (len >> 8);
  }


  /**
   * Calculates the checksum of the message contained in data.
   * 
   * @param data Raw message data.
   * 
   * @return Checksum value calculated.
   */
  public static byte calculateChecksum(byte[] data) {
    byte checksum = 0;

    final int length = getLength(data) - 1;
    for (int i = 0; i < length; ++i) {
      checksum ^= data[i + 2];
    }

    return checksum;
  }
}
