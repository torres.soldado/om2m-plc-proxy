package org.keeper.iot.wsn.plc.messages.appgenerics;

public class AppRangeAlert {
  public static final int RANGE_ALERT_ONCHANGE_DISABLE = 0;
  public static final int RANGE_ALERT_ONCHANGE_ENABLE = 1;
  
  public static final int RANGE_ALERT_TYPE_INF_LIMIT_BREACHED = 0;
  public static final int RANGE_ALERT_TYPE_SUP_LIMIT_BREACHED = 1;
  public static final int RANGE_ALERT_TYPE_CHANGE = 2;
  public static final int RANGE_ALERT_TYPE_INVALID = 3;

  /**
   * Get string representation of alert type.
   * @param alertType Alert type.
   * @return String representation of alert type.
   */
  public static String getRangeAlertTypeStr(int alertType) {
    switch (alertType) {
      case RANGE_ALERT_TYPE_INF_LIMIT_BREACHED:
        return "RANGE_ALERT_TYPE_INF_LIMIT_BREACHED";
      case RANGE_ALERT_TYPE_SUP_LIMIT_BREACHED:
        return "RANGE_ALERT_TYPE_SUP_LIMIT_BREACHED";
      case RANGE_ALERT_TYPE_CHANGE:
        return "RANGE_ALERT_TYPE_CHANGE";
      case RANGE_ALERT_TYPE_INVALID:
        return "RANGE_ALERT_TYPE_INVALID";
      default:
        return "INVALID";
    }
  }
}
