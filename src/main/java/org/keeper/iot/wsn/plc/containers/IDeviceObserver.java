package org.keeper.iot.wsn.plc.containers;

public interface IDeviceObserver {
  void onNewDevice(IContainer newManager);
}
