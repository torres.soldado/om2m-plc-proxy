/**
 * Creates a set attribute request {@link Message}.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.security.InvalidParameterException;

/**
 * @author sergio.soldado@withass.pt
 * @version $Revision: 1.0 $
 */
public class AppGenericsSetAttributeRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = AppGenericsSetAttributeRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param endpoint Target endpoint.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte endpoint, byte clusterId,
      byte attributeId, byte attributeDatatype, byte[] attributeData) {
    if (attributeData.length > 255) {
      throw new InvalidParameterException("Attribute data size to big");
    }
    final byte[] messageDat = new byte[14 + attributeData.length];
    MessageFields.setCmd0(messageDat,
        (byte) (MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_APP));
    MessageFields.setCmd1AkaClusterId(messageDat, MessageFields.APP_CLUSTER_GENERICS);
    MessageFields.setClusterCmdId(messageDat, AppGenerics.GEN_CMD_WRITE);
    MessageFields.setEndpoint(messageDat, endpoint);
    setAttNum(messageDat, (byte) 1);
    setClusterId(messageDat, clusterId);
    setAttributeId(messageDat, attributeId);
    setAttributeDatatype(messageDat, attributeDatatype);
    setAttributeDatalen(messageDat, (byte) attributeData.length);
    setAttributeData(messageDat, attributeData);
    Message message =
        new AppGenericsSetAttributeRequest(address, messageDat,
            AppGenericsSetAttributeResponse.getRegistrationId(endpoint));

    return message;
  }

  protected static void setAttNum(byte[] data, byte num) {
    data[7] = num;
  }

  protected static void setClusterId(byte[] data, byte clusterId) {
    data[8] = clusterId;
  }

  protected static byte getClusterId(byte[] data) {
    return data[8];
  }

  protected static void setAttributeId(byte[] data, byte attributeId) {
    data[9] = attributeId;
  }

  protected static byte getAttributeId(byte[] data) {
    return data[9];
  }

  protected static void setAttributeDatatype(byte[] data, byte attributeDatatype) {
    data[10] = attributeDatatype;
  }

  protected static byte getAttributeDatatype(byte[] data) {
    return data[10];
  }

  protected static void setAttributeDatalen(byte[] data, byte attributeDatalen) {
    data[11] = attributeDatalen;
  }

  protected static byte getAttributeDatalen(byte[] data) {
    return data[11];
  }

  public static byte getSeqNum(byte[] data) {
    return data[12];
  }

  public static void setSeqNum(byte[] data, byte seqNum) {
    data[12] = seqNum;
  }

  public static void setAttributeData(byte[] data, byte[] attributeData) { // TODO verify length
    Helpers.arrayToArray(attributeData, data, 0, attributeData.length, 13);
  }

  public static byte[] getAttributeData(byte[] data) {
    byte[] attributeData = new byte[data.length - 14];
    Helpers.arrayToArray(data, attributeData, 13, attributeData.length, 0);
    return attributeData;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private AppGenericsSetAttributeRequest(InetAddress address, byte[] data, int responseId) {
    super(address, data, responseId);
  }

  /**
   * Returns a string representation of message fields.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    byte clusterId = getClusterId(super.data);
    byte attributeId = getAttributeId(super.data);
    byte attributeDataType = getAttributeDatatype(super.data);
    byte attributeDatalen = getAttributeDatalen(super.data);
    byte[] attributeData = getAttributeData(super.data);

    return LOG_TAG + ": "
        + super.toString()
        + MessageDebugHelpers.printCmdClusterId(super.data)
        + MessageDebugHelpers.printEndpoint(super.data)
        + MessageDebugHelpers.printClusterId(clusterId)
        + MessageDebugHelpers.printAttributeId(attributeId)
        + MessageDebugHelpers.printAttributeDataType(attributeDataType)
        + MessageDebugHelpers.printAttributeDataLen(attributeDatalen)
        + MessageDebugHelpers.printRawData(attributeData, attributeData.length)
        + MessageDebugHelpers.printSequenceNumber(super.data);
  }
}
