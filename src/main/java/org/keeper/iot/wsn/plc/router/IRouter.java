package org.keeper.iot.wsn.plc.router;

import org.keeper.iot.wsn.plc.messages.Message;

/**
 * Message router interface.
 */
public interface IRouter {
  /**
   * Method enqueue.
   * @param msg Message
   */
  public void enqueue(Message msg);

  /**
   * Method send.
   * @param msg Message
   */
  public void send(Message msg);

  /**
   * Method onTimeout.
   * @param msg Message
   */
  public void onTimeout(Message msg);
}
